/*****
*
* File Name:  tools.gs
* File Desc:  This file contains function that shared among many Google Apps Scripts (see note 002 in README.html or README.md).
*
* Notes: 1) None right now.
*
*****/


// checks if passed number is even, returns true/false
function isEven(n) {
   return n % 2 == 0;
}


// checks if passed number is odd, returns true/false
function isOdd(n) {
   return Math.abs(n % 2) == 1;
}


// counts the number of pages in the passed-in document after getting it as a PDF
function countPages(passedDocumentObject) {
  var blob = passedDocumentObject.getAs("application/pdf");
  var data = blob.getDataAsString();
  var pages = parseInt(data.match(/ \/N (\d+) /)[1], 10);
  Logger.log("pages = " + pages);
  return pages;
}


// will return only unique values of a 1 dimesional array
function uniqueValuesFrom1DArray(inputArray) {
  var outputArray = [];
  inputArray.sort();
  outputArray.push(inputArray[0]);
  for(var n in inputArray){
    //Logger.log(outArray[outArray.length-1]+'  =  '+array[n]+' ?');
    if(outputArray[outputArray.length-1]!=inputArray[n]){
      outputArray.push(inputArray[n]);
    }
  }
  return outputArray;
}


// will modify passed in table so that all of the passed in columns for each row (minus "header" rows)
//   are changed to the passed in color
function setTableCellColumnRange(workingTableObject, columnToStart, columntoEnd, colorToSet) {
  var workingTableNumRows = workingTableObject.getNumRows();
  for (i = 2; i < workingTableNumRows; i++) {
    var workingCurrentTableRow = workingTableObject.getRow(i);
    for (j = columnToStart; j < columntoEnd + 1; j++) {
      workingCurrentTableRow.getCell(j).setText('');
      workingCurrentTableRow.getCell(j).setBackgroundColor(colorToSet);
    }
  }
}


function setGreyOutForTerm(workingTableObject,currentTerm) {
  switch (currentTerm) {
    case "T2":
      var greyOutEndRow = 4;
      break;
    case "T3":
      var greyOutEndRow = 8;
      break;
    default:
      var greyOutEndRow = "ERROR: incompatible term.";
      break;
  }
  setTableCellColumnRange(workingTableObject, 1, greyOutEndRow, '#666666');
}


/**
 *
 * Purpose: Modify passed in table object.  It can set number of rows, including header & footer.
 *   Will make alternating white/shaded columns based on groups of rows.
 * NOTE:  Counts rows from zero.
 * NOTE:  If clearing rows, does not touch header & footer.  Assumed repopulate rows outside of function.
 * NOTE:  Assumes 1st column (0) is description.  I.e. only 1 "header" column.
 * NOTE:  New rows are empty & need to be populated.
 * NOTE:  Checks for minimum number of rows to maintain formatting.
 *
 */
// NOTE:  Need to change so deal better with header & footer rows and number of columns/groups of columns
function setRowsOnTable(workingTableObject, numberOfRows, clearRows, numberOfHeaderRows, numberOfFooterRows, numberOfDataGroups, numberOfColumnsInGroup) {
  //var acculumatedErrorText = '';
  //acculumatedErrorText = acculumatedErrorText + '\nCurrentObject: ' + workingTableObject.getType() + ", " + workingTableObject.getText()
  //acculumatedErrorText = acculumatedErrorText + '\nNumber of rows: ' + numberOfRows + ", \n Number in header: " + numberOfHeaderRows + ", \n Number in footer: " + numberOfFooterRows + '.'
  //SpreadsheetApp.getUi().alert(acculumatedErrorText);
  if (numberOfRows < (3 + numberOfHeaderRows + numberOfFooterRows) ) {
    // "error" out & do nothing
    SpreadsheetApp.getUi().alert('Asked for ' + numberOfRows + ' which is too few to keep formatting with header and footer rows.');
  } else {
    var workingTableNumRows = workingTableObject.getNumRows();
    //SpreadsheetApp.getUi().alert('Asked for ' + numberOfRows + ', and ' + workingTableNumRows + ' rows exist.');
    if (workingTableNumRows < 3 + numberOfHeaderRows + numberOfFooterRows) {
      // "error" out & do nothing
      SpreadsheetApp.getUi().alert('Source table is ' + workingTableNumRows + ', so will cause problems because of formating with copy & paste.');
    } else {
      // setup style for new and/or all rows
      var styleForCellDesc = {};
      styleForCellDesc[DocumentApp.Attribute.HORIZONTAL_ALIGNMENT] = DocumentApp.HorizontalAlignment.LEFT;
      styleForCellDesc[DocumentApp.Attribute.FONT_FAMILY] = DocumentApp.FontFamily.CALIBRI;
      styleForCellDesc[DocumentApp.Attribute.FONT_SIZE] = 8;
      styleForCellDesc[DocumentApp.Attribute.BOLD] = false;
      var styleForCellMark = {};
      styleForCellMark[DocumentApp.Attribute.HORIZONTAL_ALIGNMENT] = DocumentApp.HorizontalAlignment.LEFT;
      styleForCellMark[DocumentApp.Attribute.FONT_FAMILY] = DocumentApp.FontFamily.CALIBRI;
      styleForCellMark[DocumentApp.Attribute.FONT_SIZE] = 11;
      styleForCellMark[DocumentApp.Attribute.BOLD] = false;
      // if number of existing rows is greater then needed, delete rows
      if (workingTableNumRows > numberOfRows) {
        //SpreadsheetApp.getUi().alert('Inside delete rows.');
        for (i = numberOfRows; i < workingTableNumRows; i++) {
          // assumes 2 header rows
          workingTableObject.removeRow(numberOfHeaderRows + 1);
        }
      // if number of existing rows is greater then needed, delete rows
      } else if (workingTableNumRows < numberOfRows ) {
        var numberRowsToAdd = numberOfRows - workingTableNumRows;
        //SpreadsheetApp.getUi().alert('Inside add rows; need to add:  ' + numberRowsToAdd + ' rows to ' + workingTableNumRows + ' to total ' + numberOfRows + '.');
        for (i = 1 ; i < numberRowsToAdd + 1 ; i++) {
          var workingRowToCopy = workingTableObject.getRow(numberOfHeaderRows + 1);  //to keep row formatting & counting from zero, need 2 data row
          var workingCopiedRow = workingRowToCopy.copy();  //copy the row as a "detached" object
          var workingNumRowsCurrent = workingTableObject.getNumRows();
          var workingInsertedRow = workingTableObject.insertTableRow(numberOfHeaderRows + 1, workingCopiedRow);   //now insert a table row below the first data row
          // need to insert at least 1 cell to complete row insert
          workingInsertedRow.getCell(0).setAttributes(styleForCellDesc).setText('').setBackgroundColor('#ffffff');
          // now walk through columns to set shade formatting
          var currentDataGroup = 1;
          var currentColumnOfGroup = 1;
          for (j = 1; j < ((numberOfDataGroups * numberOfColumnsInGroup) + 1 ) ; j++) {
            if ( isOdd(currentDataGroup) ) {
              workingInsertedRow.getCell(j).setAttributes(styleForCellMark).setText('').setBackgroundColor('#ffffff');
            } else {
              workingInsertedRow.getCell(j).setAttributes(styleForCellMark).setText('').setBackgroundColor('#f2f2f2');
            }
            if ( currentColumnOfGroup == numberOfColumnsInGroup ) {
              currentColumnOfGroup = 0;
              currentDataGroup++;
            }
            currentColumnOfGroup++;
          }
        }
      }
      // reset number of rows after addition/deletion
      var workingTableNumRows = workingTableObject.getNumRows();
      // now empty each row
      var checkCellGroup4 = '24';  // why 24?  I forgot what I'm doing here?!?!?!?!?!
      //SpreadsheetApp.getUi().alert('Now empty ' + workingTableNumRows + ' rows.');
      //SpreadsheetApp.getUi().alert('CurrentObject: ' + workingTableObject.getType() + ", " + workingTableObject.getText());
      for (i = numberOfHeaderRows; i < workingTableNumRows - numberOfFooterRows; i++) {
        //SpreadsheetApp.getUi().alert('Iteration ' + i + '.');
        var workingCurrentTableRow = workingTableObject.getRow(i);
        if (clearRows) {
          workingCurrentTableRow.getCell(0).setAttributes(styleForCellDesc).setText('').setBackgroundColor('#ffffff');
        } else {
          workingCurrentTableRow.getCell(0).setAttributes(styleForCellDesc).setBackgroundColor('#ffffff');
        }
        //SpreadsheetApp.getUi().alert('Iteration ' + i + ', currentObject: ' + workingCurrentTableRow.asText().getText());
        var workingTableNumCells = workingCurrentTableRow.getNumCells();
        for (j = 1; j < workingTableNumCells; j++) {
          //if (i == 2) {
            //SpreadsheetApp.getUi().alert('Cell is: ' + j + '\nDivide & round:  ' + Math.ceil(j / 4).toString() + '\nChange?  ' + checkCell4ths.indexOf(Math.ceil(j / 4).toString()));
          //};
          if (checkCellGroup4.indexOf(Math.ceil(j / 4).toString()) > -1) {
            if (clearRows) {
              workingCurrentTableRow.getCell(j).setAttributes(styleForCellMark).setText('').setBackgroundColor('#f2f2f2');
            } else {
              workingCurrentTableRow.getCell(j).setAttributes(styleForCellMark).setBackgroundColor('#f2f2f2');
            }
          } else {
            if (clearRows) {
              workingCurrentTableRow.getCell(j).setAttributes(styleForCellMark).setText('').setBackgroundColor('#ffffff');
            } else {
              workingCurrentTableRow.getCell(j).setAttributes(styleForCellMark).setBackgroundColor('#ffffff');
            }
          }
        }
      }
    }
  }
}

// returns 2D array where first arugment is passed in array, second argument is first field and third argument is second field
function extractFromArray2DArray(arrayToLookThrough, fieldToExtractFirst, fieldToExtractSecond) {
  var returnArray = [];
  for (var i in arrayToLookThrough) {
    returnArray.push([arrayToLookThrough[i][fieldToExtractFirst], arrayToLookThrough[i][fieldToExtractSecond]])
  }
  return returnArray;
}

// returns subset array of passed in array where first part of key pair contain the second argument
function extractFrom2DArrayMultipleValues(arrayToLookThrough, keyToLookFor) {
  var returnArray = [];
  for (var i in arrayToLookThrough) {
    var compareValue = arrayToLookThrough[i][0].substring(0,keyToLookFor.length);
    if (compareValue == keyToLookFor) {
      returnArray.push([arrayToLookThrough[i][0], arrayToLookThrough[i][1]])
    }
  }
  return returnArray;
}

// returns string of second part of key pair array where second argument matches exactly
function extractFrom2DArraySingleValue(arrayToLookThrough, keyToLookFor) {
  var returnValue;
  for (var i in arrayToLookThrough) {
    if (arrayToLookThrough[i][0] == keyToLookFor) {
      returnValue = arrayToLookThrough[i][1];
      return returnValue;
    }
  }
}

// returns all values in a sheet as an array
function getVariablesFromSheet(sheetToSearch) {
  var currentSpreadSheet = SpreadsheetApp.getActiveSpreadsheet();
  var currentSheet = currentSpreadSheet.getSheetByName(sheetToSearch);
  var currentRange = currentSheet.getDataRange();
  return returnArray = currentRange.getValues();
}


/**
 * Works like "key value" pair to determine what & where to save in sheet.
 * Assumes key is first column, value saved to passed in column by letter of column
 *
 */
function saveValueToSheetByUniqueColumn(sheetNameToSearch,keyToFind,valueToSave,columnToSave) {
  //SpreadsheetApp.getUi().alert('Passed parameters:  ' + sheetNameToSearch + ", " + keyToFind + ", " + valueToSave + ", " + columnToSave + ".");
  var currentSpreadSheet = SpreadsheetApp.getActiveSpreadsheet();
  var currentSheet = currentSpreadSheet.getSheetByName(sheetNameToSearch);
  var currentRange = currentSheet.getDataRange();
  var allValuesArray = currentRange.getValues();
  for (var i = 0; i<allValuesArray.length;i++){
    //SpreadsheetApp.getUi().alert('Checking:  ' + allValuesArray[i][0] + ", " + allValuesArray[i][1] + ".");
    if (allValuesArray[i][0] == keyToFind) {
      var rowOfKey = i+1;
    }
  }
  var cellNotation = columnToSave+rowOfKey
  var cellToSave = currentSheet.getRange(cellNotation);
  //SpreadsheetApp.getUi().alert('Cell and value:  ' + cellNotation + ", " + valueToSave + ".");
  cellToSave.setValue(valueToSave);
}


/**
 * Looks through passed in folder for a folder that matches passed in name
 *
 */
function existSubFolderByName(parentFolderObject,folderNameToFind) {
  var objectFound = false;
  var folderObjectChildren = parentFolderObject.getFolders();
  while (folderObjectChildren.hasNext()) {
    var folderToCheck = folderObjectChildren.next();
    var folderToCheckName = folderToCheck.getName();
    if ( folderNameToFind == folderToCheckName) {
      return true;
      break;
    }
  }
  return objectFound;
}


/**
 * Looks through passed in folder for a folder that matches passed in name.
 * Returns first found instance of folder object; dangerous as Google allows multiple folders of same name.
 * Assumes that folder exists, check for it first!
 *
 */
function findSubFolderIDByName(parentFolderObject,folderNameToFind) {
  var objectFound = false;
  var folderObjectChildren = parentFolderObject.getFolders();
  while (folderObjectChildren.hasNext()) {
    var folderToCheck = folderObjectChildren.next();
    var folderToCheckName = folderToCheck.getName();
    if ( folderNameToFind == folderToCheckName) {
      var returnFolderObject = folderToCheck;
      objectFound = true;
      break;
    }
  }
  if ( objectFound ) {
    return returnFolderObject;
  } else {
    return "ERROR, No folder found";
  }
}


/**
 * Pass in folder object and name.  If name is not found, creates subfolder.
 * Passes back folder object of either found folder or newly created folder.
 * Slightly dangerous as Google allows multiple folders of same name and only passes back first found.
 *
 */
function ensureExistSubFolderByName(parentFolderObject,folderNameToFind) {
  //SpreadsheetApp.getUi().alert('Looking in ' + parentFolderObject.getName() + ' for ' + folderNameToFind + '.');
  var objectFound = false;
  var folderObjectChildren = parentFolderObject.getFolders();
  while (folderObjectChildren.hasNext()) {
    var folderToCheck = folderObjectChildren.next();
    var folderToCheckName = folderToCheck.getName();
    if ( folderNameToFind == folderToCheckName) {
      // the first line shouldn't be necessary
      var returnFolderObject = folderToCheck;
      return folderToCheck;
      break;
    }
  }
  if ( objectFound ) {
    // this should never be called because of break above
    return returnFolderObject;
  } else {
    var returnFolderObject = parentFolderObject.createFolder(folderNameToFind);
    return returnFolderObject;
  }
}
