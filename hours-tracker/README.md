NAME:  Hours Tracker
====================

DESCRIPTION:  
-----------
A Google App Scripts Add-on to help manage a "timesheet" like Google Form & connected spreadsheet.

NOTES:
-----
1. This is a GPL v3 project.
2. It uses part of a set of Google Apps Scripts that use common code snippets (https://gitlab.com/rmdegennaro/google-apps-scripts)
3. This readme can be found as README.html in Google Apps Scripts, or as README.md in above project.

AUTHORS:
-------
rmdegennaro@gmail.com

UPDATE SUMMARIES:
----------------
* 2019-06-24, copied & made generic from a private production use.
