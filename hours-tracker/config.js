/*****
*
* File Name:  config.gs
* File Desc:  This file contains functions to get "configuration" to process data.
*             The assumption is that various sheet(s) contain configuration data.
*
* Notes: 1) None right now.
*
*****/


/**
 *
 * Purpose: Wrapper for finding data from "Misc Config" sheet.
 *
 */
function GetConfigMisc(whichConfig) {
  // defaut value
  var foundConfig = "ERROR: initialized foundConfig, but not changed by switch/case.";
  // now return config based on what asked
  switch (whichConfig) {
    case "currentYearFolderL1":
      var dataArrayFromSheet = getVariablesFromSheet('Misc Config');
      var foundConfig = extractFrom2DArraySingleValue(dataArrayFromSheet, "currentYearFolderL1");
      break;
    case "currentYearFolderL2":
      var dataArrayFromSheet = getVariablesFromSheet('Misc Config');
      var foundConfig = extractFrom2DArraySingleValue(dataArrayFromSheet, "currentYearFolderL2");
      break;
    case "templateGrade0K":
      var dataArrayFromSheet = getVariablesFromSheet('Misc Config');
      var foundConfig = extractFrom2DArraySingleValue(dataArrayFromSheet, "templateGrade0K");
      break;
    case "templateGrade01":
      var dataArrayFromSheet = getVariablesFromSheet('Misc Config');
      var foundConfig = extractFrom2DArraySingleValue(dataArrayFromSheet, "templateGrade01");
      break;
    case "templateGrade02":
      var dataArrayFromSheet = getVariablesFromSheet('Misc Config');
      var foundConfig = extractFrom2DArraySingleValue(dataArrayFromSheet, "templateGrade02");
      break;
    case "templateGrade03":
      var dataArrayFromSheet = getVariablesFromSheet('Misc Config');
      var foundConfig = extractFrom2DArraySingleValue(dataArrayFromSheet, "templateGrade03");
      break;
    case "templateGrade04":
      var dataArrayFromSheet = getVariablesFromSheet('Misc Config');
      var foundConfig = extractFrom2DArraySingleValue(dataArrayFromSheet, "templateGrade04");
      break;
    case "currentPodSelected":
      var dataArrayFromSheet = getVariablesFromSheet('Misc Config');
      var foundConfig = extractFrom2DArraySingleValue(dataArrayFromSheet, "currentPodSelected");
      break;
    case "currentYear":
      var dataArrayFromSheet = getVariablesFromSheet('Misc Config');
      var foundConfig = extractFrom2DArraySingleValue(dataArrayFromSheet, "currentYear");
      break;
    case "currentTerm":
      var dataArrayFromSheet = getVariablesFromSheet('Misc Config');
      var foundConfig = extractFrom2DArraySingleValue(dataArrayFromSheet, "currentTerm");
      break;
    case "nextTerm":
      var dataArrayFromSheet = getVariablesFromSheet('Misc Config');
      var foundConfig = extractFrom2DArraySingleValue(dataArrayFromSheet, "nextTerm");
      break;
    default:
      var foundConfig = "ERROR: asking for config that is not found.";
      break;
   }
  return foundConfig;
}


/**
 *
 * Purpose:          Finds currently selected Pod from "Misc Config" sheet & then gets other data from "Pod Config" sheet.
 * Returned values:  Pod Name (0), Pod Level (1), Pod Folder ID (2), Complete for Term (3)
 * Notes:            1) Pod Folder is essentially for current year as defined when created pod folders.
 *                   2) Pod Folder Object needs to be created after this is called.
 *
 */
function GetConfigCurrentPod() {
  var arrayCurrentPodConfig = [];
  arrayCurrentPodConfig[0] = 'ERROR:  No pod config found!';
  var arrayMiscConfig = getVariablesFromSheet('Misc Config');
  var currentPodSelected = extractFrom2DArraySingleValue(arrayMiscConfig, "currentPodSelected");
  var arrayPodConfig = getVariablesFromSheet('Pod Config');
  for (var i = 1; i < arrayPodConfig.length; i++) {
    if ( arrayPodConfig[i][0] == currentPodSelected ) {
      arrayCurrentPodConfig[0] = arrayPodConfig[i][0];
      arrayCurrentPodConfig[1] = arrayPodConfig[i][1];
      arrayCurrentPodConfig[2] = arrayPodConfig[i][2];
      arrayCurrentPodConfig[3] = arrayPodConfig[i][3];
      return arrayCurrentPodConfig;
      break;
    }
  }
  return arrayCurrentPodConfig;
}



/**
 *
 * Purpose: for passed in grade & term, return array of relevant skills
 *
 */
function GetConfigGradeTermStandards(passedGrade, passedTerm) {
  var arrayOfStandards = [];
  arrayOfStandards.push("ERROR: arrayOfStandards initialized, but not changed.");
  //
  //
  //
  if ( ( passedGrade == "3" || passedGrade == "4" ) && ( passedTerm == "T2" || passedTerm == "T3" ) ) {
    //arrayOfStandards[0] = ("Success for good grade/term for standards.");
    var arrayOfStandards = [];
    var arrayOfDataFromSheet = getVariablesFromSheet("Standards Config");
    for (var i = 1; i < arrayOfDataFromSheet.length; i++) {
      //SpreadsheetApp.getUi().alert("arrayOfDataFromSheet[i][0] " + arrayOfDataFromSheet[i][0] + "arrayOfDataFromSheet[i][1] " + arrayOfDataFromSheet[i][1] + ".");
      if ( arrayOfDataFromSheet[i][0] == passedGrade && arrayOfDataFromSheet[i][1] == passedTerm ) {
        arrayOfStandards.push(arrayOfDataFromSheet[i][2]);
      }
    }
    if ( arrayOfStandards.length == 0 ) {
      arrayOfStandards.push("ERROR: No standards in sheet for Grade " + passedGrade + " and Term " + passedTerm + ".");
    }
  } else {
    arrayOfStandards[0] = ("ERROR: Grade or term passed is undefined.  Term = " + passedTerm + ", Grade = " + passedGrade + ".");
  }
  //
  //
  //
  return(arrayOfStandards);
}
