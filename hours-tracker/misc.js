/*****
*
* File Name:  misc.gs
* File Desc:  This file is for functions that do not fit in other locations.
*
* Notes: 1) None right now.
*
*****/


// this function just removes "Copy of" from the beginning of all files in a folder, if it exists
function RemoveCopyOfAllFilesInFolder() {
  //get needed variables
  var currentClassFolderObject = DriveApp.getFolderById("1ZTK3-QgLzY91gwRO6ptwVTtRcTqE8iG1");
  var existingFiles = currentClassFolderObject.getFiles();
  while (existingFiles.hasNext()) {
    var currentFile = existingFiles.next();
    var currentFileName = currentFile.getName();
    var newFileName = currentFileName;
    var testString = newFileName.substring(0, 8);
    if ('Copy of ' == testString) {
      newFileName = newFileName.substring(8, newFileName.length);
    }
    currentFile.setName(newFileName);
  }
}


// this function is similar to changing names, just define string to search for & string to replace at the top
//  maybe change this to be generic, i.e. give list of docs & pairs to search & replace?
function ChangeStringAllReportsPerPod() {
  // what to find and what to replace
  var stringToFind = "2017 – 2018";
  var stringToReplace = "2018 – 2019";
  //get needed variables
  var currentYear = GetConfigMisc("currentYear");
  var currentTerm = GetConfigMisc("currentTerm");
  var currentClassName = GetConfigMisc("currentPodSelected");
  var currentPodConfigArray = GetConfigCurrentPod("currentClassName");
  var currentClassFolderObject = currentPodConfigArray[2];
  // other stuff
  //var newLineForPopUp = String.fromCharCode(47)
  var newLineForPopUp = "\n"

  //now do stuff
  var ui = SpreadsheetApp.getUi();
  var result = ui.alert('Start processing Pod ' + currentClassName + ' to replace: ' + newLineForPopUp + String.fromCharCode(34) + stringToFind + String.fromCharCode(34) + newLineForPopUp + 'with:' + newLineForPopUp + String.fromCharCode(34) + stringToReplace + String.fromCharCode(34) + '?', ui.ButtonSet.YES_NO);
  if (result == ui.Button.YES) {
    SpreadsheetApp.getUi().alert('Starting replacement:  ' + currentClassName + '.');
    var existingFiles = currentClassFolderObject.getFiles();
    while (existingFiles.hasNext()) {
      var currentFile = existingFiles.next();
      var currentFileName = currentFile.getName();
      var currentStudentName = currentFileName.substring(0, currentFileName.indexOf("-") - 1);
      //SpreadsheetApp.getUi().alert('Current student: ' + currentStudentName + '.');
      var currentFileId = currentFile.getId();
      var currentDocumentObject = DocumentApp.openById(currentFileId);
      //use this for body
      var currentDocumentBody = currentDocumentObject.getBody();
      currentDocumentBody.replaceText(stringToFind,stringToReplace);
      //use this for header
      var currentDocumentFooter = currentDocumentObject.getHeader();
      currentDocumentFooter.replaceText(stringToFind,stringToReplace);
      //use this for footer
      var currentDocumentFooter = currentDocumentObject.getFooter();
      currentDocumentFooter.replaceText(stringToFind,stringToReplace);
    }
    SpreadsheetApp.getUi().alert('Finished replacement:  ' + currentClassName + '.');
  }
}


/**
 * The purpose of this is a sample of grabbing data from spreadsheet
 * and insert it into a drop-down select in HTML file.
 *
 */
function getOptionsFromGS() {
  var dataArrayFromSheet = getVariablesFromSheet('Standards Config');
  var buildArray = [];
  for(var i = 0; i < dataArrayFromSheet.length; i++) {
    if (dataArrayFromSheet[i][1] == 'X1') {
      buildArray.push(dataArrayFromSheet[i][2]);
    }
  };
  return (buildArray);
}

/**
 * The purpose of this is to test code for getting individual configs from sheets
 *
 */
function popupConfigItem() {
  var pleaseFindConfig = GetConfigMisc('templateGrade02');
  SpreadsheetApp.getUi().alert('Found config is:  ' + pleaseFindConfig);
}
