/*****
*
* File Name:  system.gs
* File Desc:  This file is the main location for processing data.
*
* Notes: 1) Sidebar(s) call these functions directly.
*        2) Creating report files, sysCreateReports() & setting names sysSetStudentNames(), is
*           separated because of lenght of time; Google likes to timeout both operations for a Pod.
*        3) In sysSetStudentNames(), the student name is grabbed from file name, see above.
*        4) Some functionality is here, but incomplete.  For instance, more complex GUI stuff.
*        5) Right now a lot is in bruteforce.gs, and moving functions to be kicked off by GUI.
*
*****/


/**
 *
 * Purpose: take raw data from AdminPlus and get it ready for processing in other steps.
 * Data layout (array position):
 *   APID (0), UNIQUE ID (1), LAST NAME (2), FIRST NAME (3), COUNSELOR (4), ADVISOR (5)
 */
function sysProcessStudentData() {
  var ui = SpreadsheetApp.getUi();
  var response = ui.alert('Import data?', 'Clear lists (student, pod) and use data from Import sheet?', ui.ButtonSet.YES_NO)
  if (response == ui.Button.YES) {
    var currentSpreadsheet = SpreadsheetApp.getActiveSpreadsheet();
    var currentSheet = currentSpreadsheet.getSheetByName('Import');
    var currentRange = currentSheet.getDataRange();
    var currentData = currentRange.getValues();
    //var cleanedData = [];
    if (currentData.length > 2) {
      var destSheet = currentSpreadsheet.getSheetByName('StudentList');
      var destRange = destSheet.getDataRange();
      var destData = destRange.getValues();
      if (destData.length > 2) {
        destSheet.insertRowsAfter(destSheet.getLastRow(), 1);
        destSheet.deleteRows(2, destSheet.getLastRow() - 1);
      }
      SpreadsheetApp.setActiveSheet(destSheet);
      SpreadsheetApp.getUi().alert('Starting import of students, please wait until complete message.');
      var tempArrayPodName = [];
      var tempArrayPodLevel = [];
      for (var i = 1; i < currentData.length; i++) {
        var tempPodName = currentData[i][5] + ", " + currentData[i][4] + "'s Pod";
        var tempLevelCode = "";
        if (currentData[i][0][1] == "K") {
          tempLevelCode = "L1-K";
        } else if (currentData[i][0][1] == "1" || currentData[i][0][1] == "2") {
          tempLevelCode = "L1-NK";
        } else {
          tempLevelCode = "L2";
        }
        var tempStudentName = currentData[i][3] + ", " + currentData[i][2];
        var tempGrade = currentData[i][0][1];
        var tempUniqueID = "'" + currentData[i][1];
        var tempArray = [];
        tempArray.push(tempPodName);
        tempArray.push(tempLevelCode);
        tempArray.push(tempStudentName);
        tempArray.push(tempGrade);
        tempArray.push(tempUniqueID);
        tempArrayPodName.push(tempPodName);
        tempArrayPodLevel.push(tempLevelCode);
        destSheet.appendRow(tempArray);
        SpreadsheetApp.flush();
      }
      SpreadsheetApp.getUi().alert('Starting creation of pods, please wait until complete message.');
      var arrayPodNameLevel = [];
      var tempArrayEntry = [];
      var tempArrayFoundPods = [];
      tempArrayFoundPods.push(tempArrayPodName[0]);
      tempArrayEntry.push(tempArrayPodName[0]);
      tempArrayEntry.push(tempArrayPodLevel[0]);
      arrayPodNameLevel.push(tempArrayEntry);
      for (var i = 1; i < tempArrayPodName.length; i++) {
        if (tempArrayFoundPods.indexOf(tempArrayPodName[i]) == -1 ) {
          tempArrayFoundPods.push(tempArrayPodName[i]);
          var tempArrayEntry = [];
          tempArrayEntry.push(tempArrayPodName[i]);
          tempArrayEntry.push(tempArrayPodLevel[i]);
          arrayPodNameLevel.push(tempArrayEntry);
        }
      }
      //SpreadsheetApp.getUi().alert('Processed pod names are:  ' + arrayPodNameLevel);
      var destSheet = currentSpreadsheet.getSheetByName('Pod Config');
      var destRange = destSheet.getDataRange();
      var destData = destRange.getValues();
      if (destData.length > 2) {
        destSheet.insertRowsAfter(destSheet.getLastRow(), 1);
        destSheet.deleteRows(2, destSheet.getLastRow() - 1);
      }
      SpreadsheetApp.setActiveSheet(destSheet);
      for (var i = 0; i < arrayPodNameLevel.length; i++) {
        destSheet.appendRow(arrayPodNameLevel[i]);
        SpreadsheetApp.flush();
      }
      SpreadsheetApp.getUi().alert('Import completed; please review data & refresh controls.');
    } else {
      SpreadsheetApp.getUi().alert('Error, no data in Import sheet.');
    }
  } else {
    SpreadsheetApp.getUi().alert('Import cancelled.');
  }
}

/**
 *
 * Purpose: create report file per student based on templates.
 * Note: 1st - 4th use T1/2/3.  Kindergarten is not using Progress Reports FY2019.
 *
 */
function sysCreatePods() {
  var ui = SpreadsheetApp.getUi();
  var response = ui.alert('Create Folders?', 'Create Year & Pod folders for Current Year?', ui.ButtonSet.YES_NO)
  if (response == ui.Button.YES) {
    // find/create parent folders
    SpreadsheetApp.getUi().alert('Checking for Year folders, please wait until complete message.');
    var currentYear = GetConfigMisc("currentYear");
    var folderIDLevel1 = GetConfigMisc("currentYearFolderL1");
    var folderObjectLevel1 = DriveApp.getFolderById(folderIDLevel1);
    var currentYearFolderObjectLevel1 = ensureExistSubFolderByName(folderObjectLevel1,currentYear);
    var currentYearFolderObjectKs = ensureExistSubFolderByName(currentYearFolderObjectLevel1,"K's");
    var currentYearFolderObject12s = ensureExistSubFolderByName(currentYearFolderObjectLevel1,"1st & 2nd's");
    var folderIDLevel2 = GetConfigMisc("currentYearFolderL2");
    var folderObjectLevel2 = DriveApp.getFolderById(folderIDLevel2);
    var currentYearFolderObjectLevel2 = ensureExistSubFolderByName(folderObjectLevel2,currentYear);
    // step through pod list and create pod folders
    var currentSpreadsheet = SpreadsheetApp.getActiveSpreadsheet();
    var currentSheet = currentSpreadsheet.getSheetByName('Pod Config');
    var currentRange = currentSheet.getDataRange();
    var currentData = currentRange.getValues();
    SpreadsheetApp.setActiveSheet(currentSheet);
    SpreadsheetApp.getUi().alert('Starting creation of Pod folders, please wait until complete message.');
    for (var i = 1; i < currentData.length; i++) {
      if ( currentData[i][1] == 'L1-K' ) {
        var currentPodFolderObject = ensureExistSubFolderByName(currentYearFolderObjectKs,currentData[i][0]);
      } else if ( currentData[i][1] == 'L1-NK' ) {
        var currentPodFolderObject = ensureExistSubFolderByName(currentYearFolderObject12s,currentData[i][0]);
      } else if ( currentData[i][1] == 'L2' ) {
        var currentPodFolderObject = ensureExistSubFolderByName(currentYearFolderObjectLevel2,currentData[i][0]);
      } else {
        SpreadsheetApp.getUi().alert('ERROR!  The following Level is undefined:  ' + currentData[i][1] + ".");
      }
      var currentPodFolderID = currentPodFolderObject.getId();
      saveValueToSheetByUniqueColumn("Pod Config",currentData[i][0],currentPodFolderID,"C");
      saveValueToSheetByUniqueColumn("Pod Config",currentData[i][0],currentYear + " - Pod folder created","D");
    }
    SpreadsheetApp.getUi().alert('Folders created ' + currentYear + " for " + currentClassName + ".");
  } else {
    SpreadsheetApp.getUi().alert('Folders creation cancelled.');
  }
}

/**
 *
 * Purpose: create report file per student based on templates.
 * Note: Not stripping out unneeded pods because then only loop through StudentList data once.
 *
 */
function sysCreateReports() {
  SpreadsheetApp.setActiveSheet(SpreadsheetApp.getActiveSpreadsheet().getSheetByName('Pod Config'));
  var currentYear = GetConfigMisc("currentYear");
  var currentTerm = GetConfigMisc("currentTerm");
  //get pod related variables
  var arrayCurrentPodConfig = GetConfigCurrentPod();
  var currentClassName = arrayCurrentPodConfig[0];
  var currentClassLevel = arrayCurrentPodConfig[1];
  var currentClassFolderID = arrayCurrentPodConfig[2];
  var currentClassFolderObject = DriveApp.getFolderById(currentClassFolderID);
  var ui = SpreadsheetApp.getUi();
  var response = ui.alert('Create Reports?', 'Create reports for ' + currentYear + "-" + currentTerm + " for " + currentClassName + '?', ui.ButtonSet.YES_NO)
  if (response == ui.Button.YES) {
    SpreadsheetApp.getUi().alert('Starting creation of reports for ' + currentClassName + '.  This will take some time; please wait until complete message.');
    //get needed variables "Misc Variables"
    var templateGrade0K = GetConfigMisc('templateGrade0K');
    var templateGrade01 = GetConfigMisc('templateGrade01');
    var templateGrade02 = GetConfigMisc('templateGrade02');
    var templateGrade03 = GetConfigMisc('templateGrade03');
    var templateGrade04 = GetConfigMisc('templateGrade04');
    var currentSpreadsheet = SpreadsheetApp.getActiveSpreadsheet();
    var currentSheet = currentSpreadsheet.getSheetByName('StudentList');
    var currentRange = currentSheet.getDataRange();
    var data = currentRange.getValues();
    for (i in data) {
      var currentStudent = data[i];
      var currentStudentPod = currentStudent[0];
      var currentStudentLevel = currentStudent[1];
      var currentStudentName = currentStudent[2];
      var currentStudentGrade = currentStudent[3].toString();
      if (currentStudentPod == currentClassName) {
        switch (currentStudentGrade) {
          case "K":
            var newFileName = currentStudentName + ' - Kindergarten Progress Report ' + currentYear + ', ' + currentTerm;
            DriveApp.getFileById(templateGrade0K).makeCopy(newFileName,currentClassFolderObject);
            break;
          case "1":
            var newFileName = currentStudentName + ' - 1st Grade Progress Report ' + currentYear + ', ' + currentTerm;
            DriveApp.getFileById(templateGrade01).makeCopy(newFileName,currentClassFolderObject);
            break;
          case "2":
            var newFileName = currentStudentName + ' - 2nd Grade Progress Report ' + currentYear + ', ' + currentTerm;
            DriveApp.getFileById(templateGrade02).makeCopy(newFileName,currentClassFolderObject);
            break;
          case "3":
            var newFileName = currentStudentName + ' - 3rd Grade Progress Report ' + currentYear + ', ' + currentTerm;
            DriveApp.getFileById(templateGrade03).makeCopy(newFileName,currentClassFolderObject);
            break;
          case "4":
            var newFileName = currentStudentName + ' - 4th Grade Progress Report ' + currentYear + ', '+ currentTerm;
            DriveApp.getFileById(templateGrade04).makeCopy(newFileName,currentClassFolderObject);
            break;
          default:
            SpreadsheetApp.getUi().alert('Grade ' + currentStudentGrade + ' for ' + currentStudentName + ' is invalid!!!');
            break;
        }
      }
    }
    saveValueToSheetByUniqueColumn("Pod Config",currentClassName,currentYear + " - Reports created","D");
    SpreadsheetApp.getUi().alert('Reports created ' + currentYear + "-" + currentTerm + " for " + currentClassName + ".");
  } else {
    SpreadsheetApp.getUi().alert('Report creation cancelled.');
  }
}

/**
 *
 * Purpose: sets names inside the file based on the file name.
 * Note:
 *
 */
function sysSetStudentNames() {
  SpreadsheetApp.setActiveSheet(SpreadsheetApp.getActiveSpreadsheet().getSheetByName('Pod Config'));
  var currentYear = GetConfigMisc("currentYear");
  var currentTerm = GetConfigMisc("currentTerm");
  //get pod related variables
  var arrayCurrentPodConfig = GetConfigCurrentPod();
  var currentClassName = arrayCurrentPodConfig[0];
  var currentClassLevel = arrayCurrentPodConfig[1];
  var currentClassFolderID = arrayCurrentPodConfig[2];
  var currentClassFolderObject = DriveApp.getFolderById(currentClassFolderID);
  var ui = SpreadsheetApp.getUi();
  var response = ui.alert('Merge Names?', 'Merge student names for ' + currentYear + "-" + currentTerm + " for " + currentClassName + '?', ui.ButtonSet.YES_NO)
  if (response == ui.Button.YES) {
    SpreadsheetApp.getUi().alert('Starting name merging for ' + currentClassName + '.  This will take some time; please wait until complete message.');
    var existingFiles = currentClassFolderObject.getFiles();
    while (existingFiles.hasNext()) {
      var currentFile = existingFiles.next();
      var currentFileName = currentFile.getName();
      var currentStudentName = currentFileName.substring(0, currentFileName.indexOf(" - "));
      //SpreadsheetApp.getUi().alert('Current student: ' + currentStudentName + '.');
      var currentFileId = currentFile.getId();
      var currentDocumentObject = DocumentApp.openById(currentFileId);
      var currentDocumentBody = currentDocumentObject.getBody();
      var currentDocumentFooter = currentDocumentObject.getFooter();
      currentDocumentBody.replaceText('{{STUDENT NAME}}', currentStudentName);
      currentDocumentFooter.replaceText('{{STUDENT NAME}}', currentStudentName);
      currentDocumentFooter.replaceText('{{POD NAME}}', currentClassName);
    }
    saveValueToSheetByUniqueColumn("Pod Config",currentClassName,currentYear + " - Names merged","D");
    SpreadsheetApp.getUi().alert('Names merged for ' + currentYear + "-" + currentTerm + " for " + currentClassName + ".");
  } else {
    SpreadsheetApp.getUi().alert('Merging names cancelled.');
  }
}

/**
 *
 * Purpose: makes backup of file named for currentTerm and renames file to nextTerm
 * Note:
 *
 */
function sysArchiveReports() {
  SpreadsheetApp.setActiveSheet(SpreadsheetApp.getActiveSpreadsheet().getSheetByName('Pod Config'));
  var currentYear = GetConfigMisc("currentYear");
  var currentTerm = GetConfigMisc("currentTerm");
  var nextTerm = GetConfigMisc("nextTerm");
  //get pod related variables
  var arrayCurrentPodConfig = GetConfigCurrentPod();
  var currentClassName = arrayCurrentPodConfig[0];
  var currentClassLevel = arrayCurrentPodConfig[1];
  var currentClassFolderID = arrayCurrentPodConfig[2];
  var currentClassFolderObject = DriveApp.getFolderById(currentClassFolderID);
  var ui = SpreadsheetApp.getUi();
  var response = ui.alert('Archive Reports?', 'Archive reports for ' + currentYear + "-" + currentTerm + " for " + currentClassName + '?', ui.ButtonSet.YES_NO)
  if (response == ui.Button.YES) {
    SpreadsheetApp.getUi().alert('Archiving reports for ' + currentClassName + '.  This will take some time; please wait until complete message.');
    var foundTermArchiveFolder = false;
    var currentArchiveFolder;
    var existingSubfolders = currentClassFolderObject.getFolders();
    while (existingSubfolders.hasNext()) {
      var currentFolder = existingSubfolders.next();
      var currentFolderName = currentFolder.getName();
      if ( currentFolderName == currentTerm + " Archive" ) {
        foundTermArchiveFolder = true;
        currentArchiveFolder = currentFolder;
      }
    }
    if ( foundTermArchiveFolder == false ) {
      currentArchiveFolder = currentClassFolderObject.createFolder(currentTerm + " Archive");
    }
    currentArchiveFolder.setShareableByEditors(false);
    var existingFiles = currentClassFolderObject.getFiles();
    while (existingFiles.hasNext()) {
      var currentFile = existingFiles.next();
      var currentFileName = currentFile.getName();
      var newFileObject = currentFile.makeCopy(currentFileName, currentArchiveFolder);
      currentFile.setName(currentFileName.substring(0, currentFileName.length - 4) + ", " + nextTerm);
    }
    saveValueToSheetByUniqueColumn("Pod Config",currentClassName,currentYear + "-" + currentTerm + " - Archived","D");
    SpreadsheetApp.getUi().alert('Reports archived for ' + currentYear + "-" + currentTerm + " for " + currentClassName + ".");
  } else {
    SpreadsheetApp.getUi().alert('Archive reports cancelled.');
  }
}

/**
 *
 * Purpose: For 3rd & 4th grade, math standards change quarterly.  This adjusts that table.
 *
 */
function sysChangeMathStandards() {
  SpreadsheetApp.setActiveSheet(SpreadsheetApp.getActiveSpreadsheet().getSheetByName('Pod Config'));
  var currentYear = GetConfigMisc("currentYear");
  var currentTerm = GetConfigMisc("currentTerm");
  var nextTerm = GetConfigMisc("nextTerm");
  //get pod related variables
  var arrayCurrentPodConfig = GetConfigCurrentPod();
  var currentClassName = arrayCurrentPodConfig[0];
  var currentClassLevel = arrayCurrentPodConfig[1];
  var currentClassFolderID = arrayCurrentPodConfig[2];
  var currentClassFolderObject = DriveApp.getFolderById(currentClassFolderID);
  if ( currentClassLevel == 'L2' && ( currentTerm == "T2"  || currentTerm == "T3")) {
    var ui = SpreadsheetApp.getUi();
    var response = ui.alert('Set Math Standards?', 'Change math standards for ' + currentYear + "-" + currentTerm + " for " + currentClassName + '?', ui.ButtonSet.YES_NO)
    if (response == ui.Button.YES) {
      SpreadsheetApp.getUi().alert("WARNING: A teacher may accidentally modify table, there may be errors at the end.");
      SpreadsheetApp.getUi().alert('Changing math standards for ' + currentClassName + '.  This will take some time; please wait until complete message.');
      var arrayOfStandards3rdMath = GetConfigGradeTermStandards('3', currentTerm);
      var arrayOfStandards3rdMathLength = arrayOfStandards3rdMath.length;
      var arrayOfStandards4thMath = GetConfigGradeTermStandards('4', currentTerm);
      var arrayOfStandards4thMathLength = arrayOfStandards4thMath.length;
      if ( arrayOfStandards3rdMath[0].substring(0,5) == "ERROR" || arrayOfStandards4thMath[0].substring(0,5) == "ERROR" ) {
        SpreadsheetApp.getUi().alert("ERROR: One of the 3rd or 4th grade standards has an error.\n" + arrayOfStandards3rdMath + "\n" + arrayOfStandards4thMath);
        saveValueToSheetByUniqueColumn("Pod Config",currentClassName,currentYear + "-" + currentTerm + " - Error w/ Math standards","D");
      } else {
        var existingFiles = currentClassFolderObject.getFiles();
        while (existingFiles.hasNext()) {
          var rowsToHaveInTable = 0;
          var currentFileObject = existingFiles.next();
          var currentFileName = currentFileObject.getName();
          var currentFileID = currentFileObject.getId();
          var currentStudentName = currentFileName.substring(0, currentFileName.indexOf("-") - 1);
          var currentDocObject = DocumentApp.openById(currentFileID);
          var currentDocObjectHeader = currentDocObject.getHeader().copy();
          // find grade
          var workingGradeElement = currentDocObjectHeader.findText(' Progress Report').getElement();
          var workingGradeAsText = workingGradeElement.asText().getText().trim().substring(0,1);
          // nothing to do for K - 2 graders
          if (workingGradeAsText == '3' || workingGradeAsText == '4') {
            // take into account two header rows & 1 footer/comments row
            if (workingGradeAsText == '3') {
              var rowsToHaveInTable = arrayOfStandards3rdMathLength + 3;
            } else if (workingGradeAsText == '4') {
              var rowsToHaveInTable = arrayOfStandards4thMathLength + 3;
            } else {
              SpreadsheetApp.getUi().alert("ERROR: Asked for grade with no standards to change.");
            }
            var currentDocObjectBody = currentDocObject.getBody();
            var workingTableObject = currentDocObjectBody.findText('Math').getElement().getParent().getParent().getParent().getParent();
            setRowsOnTable(workingTableObject, rowsToHaveInTable, true, 2, 1, 3, 4);
            var styleForCellMark = {};
            styleForCellMark[DocumentApp.Attribute.HORIZONTAL_ALIGNMENT] = DocumentApp.HorizontalAlignment.LEFT;
            styleForCellMark[DocumentApp.Attribute.FONT_FAMILY] = DocumentApp.FontFamily.CALIBRI;
            styleForCellMark[DocumentApp.Attribute.FONT_SIZE] = 8;
            styleForCellMark[DocumentApp.Attribute.BOLD] = false;
            if (workingGradeAsText == '3') {
              for (i = 0; i < arrayOfStandards3rdMathLength; i++) {
                workingTableObject.getRow(i+2).getCell(0).setAttributes(styleForCellMark).setText(arrayOfStandards3rdMath[i]);
              }
            } else {
              for (i = 0; i < arrayOfStandards4thMathLength; i++) {
                workingTableObject.getRow(i+2).getCell(0).setAttributes(styleForCellMark).setText(arrayOfStandards4thMath[i]);
              }
            }
            setGreyOutForTerm(workingTableObject, currentTerm);
          }
        }
        saveValueToSheetByUniqueColumn("Pod Config",currentClassName,currentYear + "-" + currentTerm + " - Math standards set","D");
        SpreadsheetApp.getUi().alert('Math standards set for ' + currentYear + "-" + currentTerm + " for " + currentClassName + ".");
      }
    } else {
      SpreadsheetApp.getUi().alert('Setting Math standards cancelled.');
    }
  } else {
    SpreadsheetApp.getUi().alert('Math standards change only for terms other then first term on Level 2; nothing to do. :-)');
  }
}

/**
 *
 * Purpose: For the selected Pod, will create a folder BACKUP-YYYYMMDDhhmmss and copy all
 * the report files to that folder.
 *
 */
function sysBackupReports() {
  //get pod related variables
  var arrayCurrentPodConfig = GetConfigCurrentPod();
  var currentClassName = arrayCurrentPodConfig[0];
  var currentClassLevel = arrayCurrentPodConfig[1];
  var currentClassFolderID = arrayCurrentPodConfig[2];
  var currentClassFolderObject = DriveApp.getFolderById(currentClassFolderID);
  var currentDateTime = new Date();
  var currentTimeStamp = currentDateTime.getYear().toString() + ('0' + (currentDateTime.getMonth()+1)).slice(-2) + ('0' + (currentDateTime.getDate())).slice(-2) + ('0' + (currentDateTime.getHours())).slice(-2) + ('0' + (currentDateTime.getMinutes())).slice(-2) + ('0' + (currentDateTime.getSeconds())).slice(-2);
  var backupFolderName = "Backup-" + currentTimeStamp;
  var ui = SpreadsheetApp.getUi();
  var response = ui.alert('Backup Reports?', 'Backup reports for ' + currentClassName + ' using folder of ' + backupFolderName + '?', ui.ButtonSet.YES_NO)
  if (response == ui.Button.YES) {
    SpreadsheetApp.getUi().alert('Backing up reports for ' + currentClassName + 'to folder ' + backupFolderName + '.  This will take some time; please wait until complete message.');
    var foundTermArchiveFolder = false;
    var currentArchiveFolder;
    var existingSubfolders = currentClassFolderObject.getFolders();
    while (existingSubfolders.hasNext()) {
      var currentFolder = existingSubfolders.next();
      var currentFolderName = currentFolder.getName();
      if ( currentFolderName == backupFolderName ) {
        foundTermArchiveFolder = true;
        currentArchiveFolder = currentFolder;
      }
    }
    if ( foundTermArchiveFolder == false ) {
      currentArchiveFolder = currentClassFolderObject.createFolder(backupFolderName);
    }
    currentArchiveFolder.setShareableByEditors(false);
    var existingFiles = currentClassFolderObject.getFiles();
    while (existingFiles.hasNext()) {
      var currentFile = existingFiles.next();
      var currentFileName = currentFile.getName();
      var newFileObject = currentFile.makeCopy(currentFileName, currentArchiveFolder);
    }
    SpreadsheetApp.getUi().alert('Reports backed up for ' + currentClassName + ".");
  } else {
    SpreadsheetApp.getUi().alert('Archive reports cancelled.');
  }
}

/**
 *
 * Purpose: User will select folder & if matches certain criteria, will copy the files from
 * that folder to the parent folder.
 *
 */
function sysRestoreReports() {
  SpreadsheetApp.getUi().alert("I lied in the sheet.  This requires a Sidebar to choose from existing backups.  I.e. alot more code then I have time to write during the summer.  \n\nSorry.  :-(");
}

/**
 *
 * Purpose: User will select Google Doc for templated to be used for the level; the ID of the
 * doc will be saved to the correct sheet/item.
 *
 */
function sysSetTemplate(selectedGrade) {
  // need Google Picker API, so tabling until have some time to devote to figuring it out
  SpreadsheetApp.setActiveSheet(SpreadsheetApp.getActiveSpreadsheet().getSheetByName("Misc Config"));
  SpreadsheetApp.getUi().alert("I lied in the sheet.  Google Picker code is not written. In 'Misc Config', please manually enter template ID for Grade:  " + selectedGrade + ".");
}

/**
 *
 * Purpose: User will select Google Drive folder to be used for the level; the ID of the
 * folder will be saved to the correct sheet/item.
 *
 */
function sysSetFolderLevel(selectedLevel) {
  // need Google Picker API, so tabling until have some time to devote to figuring it out
  SpreadsheetApp.setActiveSheet(SpreadsheetApp.getActiveSpreadsheet().getSheetByName("Misc Config"));
  SpreadsheetApp.getUi().alert("I lied in the sheet.  Google Picker code is not written. In 'Misc Config', please manually enter folder ID for Level:  " + selectedLevel + ".");
}

/**
 *
 * Purpose: User will select Google Drive folder to be used for the level; the ID of the
 * folder will be saved to the correct sheet/item.
 *
 */
function sysSearchReplaceByPod(stringToFind, stringToReplace) {
  var proceedWithReplace = false;
  if ( stringToFind == '' ) {
      SpreadsheetApp.getUi().alert("ERROR: No text to search for.");
  } else if ( stringToReplace == '' ) {
    var response = ui.alert('Proceed?', 'Replace field is empty, this will remove:' + stringToFind + '. Are you sure?', ui.ButtonSet.YES_NO)
    if (response == ui.Button.YES) {
      proceedWithReplace = true;
    }
  } else {
    proceedWithReplace = true;
  }
  if ( proceedWithReplace == true ) {

//
    //get needed variables
    var currentYear = GetConfigMisc("currentYear");
    var currentTerm = GetConfigMisc("currentTerm");
    var currentClassName = GetConfigMisc("currentPodSelected");
    var currentPodConfigArray = GetConfigCurrentPod("currentClassName");
    var currentClassFolderID = currentPodConfigArray[2];
    var currentClassFolderObject = DriveApp.getFolderById(currentClassFolderID);
    // other stuff
    //var newLineForPopUp = String.fromCharCode(47)
    var newLineForPopUp = "\n"

    //now do stuff
    var ui = SpreadsheetApp.getUi();
    var result = ui.alert('Start processing Pod ' + currentClassName + ' to replace: ' + newLineForPopUp + String.fromCharCode(34) + stringToFind + String.fromCharCode(34) + newLineForPopUp + 'with:' + newLineForPopUp + String.fromCharCode(34) + stringToReplace + String.fromCharCode(34) + '?', ui.ButtonSet.YES_NO);
    if (result == ui.Button.YES) {
      SpreadsheetApp.getUi().alert('Starting replacement:  ' + currentClassName + '.');
      var existingFiles = currentClassFolderObject.getFiles();
      while (existingFiles.hasNext()) {
        var currentFile = existingFiles.next();
        var currentFileName = currentFile.getName();
        var currentStudentName = currentFileName.substring(0, currentFileName.indexOf("-") - 1);
        //SpreadsheetApp.getUi().alert('Current student: ' + currentStudentName + '.');
        var currentFileId = currentFile.getId();
        var currentDocumentObject = DocumentApp.openById(currentFileId);
        //use this for body
        var currentDocumentBody = currentDocumentObject.getBody();
        currentDocumentBody.replaceText(stringToFind,stringToReplace);
        //use this for header
        var currentDocumentFooter = currentDocumentObject.getHeader();
        currentDocumentFooter.replaceText(stringToFind,stringToReplace);
        //use this for footer
        var currentDocumentFooter = currentDocumentObject.getFooter();
        currentDocumentFooter.replaceText(stringToFind,stringToReplace);
      }
      SpreadsheetApp.getUi().alert('Finished replacement:  ' + currentClassName + '.');
    }
//

  }
}
