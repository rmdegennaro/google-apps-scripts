/*****
*
* File Name:  ui.gs
* File Desc:  This file deals with connecting to sidebar HTML files.
*
* Notes: 1) Calls functions to build the sidebars.
*        2) Calls functions to setup of menu items in regular Sheets GUI.
*
*****/


/**
 * Creates a menu entry in the Google Docs UI when the document is opened.
 *
 * @param {object} e The event parameter for a simple onOpen trigger. To
 *     determine which authorization mode (ScriptApp.AuthMode) the trigger is
 *     running in, inspect e.authMode.
 */
function onOpen(e) {
  // create menu item to open Plugin sidebar
  SpreadsheetApp.getUi().createAddonMenu()
      .addItem('Start', 'showSidebar')
      .addToUi();
  SpreadsheetApp.getUi().createMenu("Progress Reports")
      .addItem('Start', 'showSidebar')
      .addToUi();
}

/**
 * Runs when the add-on is installed.
 *
 * @param {object} e The event parameter for a simple onInstall trigger. To
 *     determine which authorization mode (ScriptApp.AuthMode) the trigger is
 *     running in, inspect e.authMode. (In practice, onInstall triggers always
 *     run in AuthMode.FULL, but onOpen triggers may be AuthMode.LIMITED or
 *     AuthMode.NONE.)
 */
function onInstall(e) {
  onOpen(e);
}

/**
 * Used to open only the first sidebar.
 */
function showSidebar() {
  var ui = HtmlService.createHtmlOutputFromFile('Sidebar01')
      .setTitle('Progress Report Start');
  SpreadsheetApp.getUi().showSidebar(ui);
}


/**
 * The purpose of this is grab data from spreadsheet and insert it into a
 * drop-down select in HTML file.
 * Also find saved value in index of terms and tack it on the end of array.
 *
 */
function getSelectYearOptionsFromSheet() {
  // setup data variables
  var buildArray = [];
  var yearOptionsArrayFromSheet = getVariablesFromSheet('Static Config');
  var miscConfigArrayFromSheet = getVariablesFromSheet('Misc Config');

  // get list of options for select
  for(var i = 0; i < yearOptionsArrayFromSheet.length; i++) {
    if (yearOptionsArrayFromSheet[i][0] == 'years') {
      buildArray.push(yearOptionsArrayFromSheet[i][1]);
    }
  };

  // get from sheet the saved value for currentTerm
  for(var i = 0; i < miscConfigArrayFromSheet.length; i++) {
    if (miscConfigArrayFromSheet[i][0] == 'currentYear') {
      var workinYear = miscConfigArrayFromSheet[i][1];
    }
  }
  // find index where current term value matches in buildArray
  for(var i = 0; i < buildArray.length; i++) {
    if (buildArray[i] == workinYear) {
      var storedWorkingYearIndex = i;
      }
  }
  // add that index to end of array
  buildArray.push(storedWorkingYearIndex);

  // return both sets of values
  //SpreadsheetApp.getUi().alert('buildArray:  ' + buildArray);
  return (buildArray);
}


/**
 * The purpose of this is to write the Year from HTML file
 * to the appropriate sheet.
 *
 */
function saveSelectedYearToSheet(passedArray) {
  var yearToBeSaved = passedArray[0];
  saveValueToSheetByUniqueColumn('Misc Config','currentYear',yearToBeSaved,'B');
}


/**
 * The purpose of this is grab data from spreadsheet and insert it into a
 * drop-down select in HTML file.
 * Also find saved value in index of terms and tack it on the end of array.
 *
 */
function getSelectTermOptionsFromSheet() {
  // setup data variables
  var buildArray = [];
  var termOptionsArrayFromSheet = getVariablesFromSheet('Static Config');
  var miscConfigArrayFromSheet = getVariablesFromSheet('Misc Config');

  // get list of options for select
  for(var i = 0; i < termOptionsArrayFromSheet.length; i++) {
    if (termOptionsArrayFromSheet[i][0] == 'terms') {
      buildArray.push(termOptionsArrayFromSheet[i][1]);
    }
  };

  // get from sheet the saved value for currentTerm
  for(var i = 0; i < miscConfigArrayFromSheet.length; i++) {
    if (miscConfigArrayFromSheet[i][0] == 'currentTerm') {
      var workinTerm = miscConfigArrayFromSheet[i][1];
    }
  }
  // find index where current term value matches in buildArray
  for(var i = 0; i < buildArray.length; i++) {
    if (buildArray[i] == workinTerm) {
      var storedWorkingTermIndex = i;
      }
  }
  // add that index to end of array
  buildArray.push(storedWorkingTermIndex);

  // return both sets of values
  return (buildArray);
}


/**
 * The purpose of this is to write the Terms from HTML file
 * to the appropriate sheet.
 *
 */
function saveSelectedTermsToSheet(passedArray) {
  //SpreadsheetApp.getUi().alert('Passed array:  ' + passedArray);
  var termToBeSavedCurrent = passedArray[0];
  var termToBeSavedNext = passedArray[1];
  saveValueToSheetByUniqueColumn('Misc Config','currentTerm',termToBeSavedCurrent,'B');
  saveValueToSheetByUniqueColumn('Misc Config','nextTerm',termToBeSavedNext,'B');
}


/**
 * The purpose of this is to grab list of Pods from spreadsheet
 * and insert it into a drop-down select in HTML file.
 *
 */
function getSelectPodOptionsFromSheet() {
  // setup data variables
  var buildArray = [];
  var podArrayFromSheet = getVariablesFromSheet('Pod Config');
  var miscConfigArrayFromSheet = getVariablesFromSheet('Misc Config');

  // get list of options for select
  var buildArray = [];
  for(var i = 1; i < podArrayFromSheet.length; i++) {
    buildArray.push(podArrayFromSheet[i][0]);
  }

  // get from sheet the saved value for currentTerm
  for(var i = 0; i < miscConfigArrayFromSheet.length; i++) {
    if (miscConfigArrayFromSheet[i][0] == 'currentPodSelected') {
      var workinPod = miscConfigArrayFromSheet[i][1];
    }
  }
  // find index where current term value matches in buildArray
  for(var i = 0; i < buildArray.length; i++) {
    if (buildArray[i] == workinPod) {
      var storedWorkingPodIndex = i;
      }
  }
  // add that index to end of array
  buildArray.push(storedWorkingPodIndex);

  // return both sets of values
  return (buildArray);
}


/**
 * The purpose of this is to write the Pod from HTML file
 * to the appropriate sheet.
 *
 */
function saveSelectedPodToSheet(passedArray) {
  //SpreadsheetApp.getUi().alert('Passed array:  ' + passedArray);
  var podToBeSaved = passedArray[0];
  saveValueToSheetByUniqueColumn('Misc Config','currentPodSelected',podToBeSaved,'B');
}
